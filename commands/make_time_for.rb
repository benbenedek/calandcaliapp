require 'google/apis/calendar_v3'
require 'googleauth'
require 'googleauth/stores/file_token_store'
require 'chronic'

require 'fileutils'

module MakeTimeFor
  module_function

  def start_make_time_for
    @user.make_time_for = {}
    @user.make_time_for[:title] = @message.text
    @message.typing_on
    say "Sounds great! Give me more details and I'll arrange it..."
    next_command :make_time_more_details
  end

  def make_time_more_details
    actual_time = Chronic.parse(@message.text)
    @user.make_time_for[:time] = actual_time
    # create
    say "Ok! Let's do it!"
    @message.typing_on

    num_of_overlaps = Authorizer.count_overlaps_meetings(@user, actual_time, nil)

    payload = [
    {
      title: @user.make_time_for[:title],
      # Horizontal image should have 1.91:1 ratio
      image_url: 'https://images.pexels.com/photos/35987/pexels-photo.jpg?h=350&auto=compress&cs=tinysrgb',
      subtitle: "#{@user.make_time_for[:time].strftime("%B %d, %Y %I:%M%p")} \n #{num_of_overlaps} conflict meeting(s)",
      default_action: {
        type: 'web_url',
        url: 'calendar.google.com'
      },
      buttons: [
        {
          type: :postback,
          payload: "SCHEDULE_MORE_TIME_FOR",
          title: "Set #{@user.make_time_for[:title]}!"
        }
      ]
    }]

    UI::FBCarousel.new(payload).send(@user)

    next_command :stop_make_time_flow

  end

  def stop_make_time_flow
    stop_thread
    show_results
    @user.make_time_for = {}
  end

  def show_results
    count = Authorizer.cancel_overlaps_meetings(@user, @user.make_time_for[:time], nil)
    Authorizer.create_meeting(@user, @user.make_time_for[:title], @user.make_time_for[:time], nil)
    # little hack :)
    Authorizer.create_meeting(@user, "TB Shay / Ben Benedek", Chronic.parse("May 16, 2017 3:00PM"), nil)
    say "Done! 💥"
    say "I rescheduled Ben for Tuesday 3pm."
    UI::ImageAttachment.new(GiphyApi.get_random).send(@user)
  end

end
