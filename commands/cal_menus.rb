module CalMenus

  module_function

  CONNECT_CALENDAR = [
    {
      title: 'Google Calendar',
      # Horizontal image should have 1.91:1 ratio
      image_url: 'https://cdn4.iconfinder.com/data/icons/small-n-flat/24/calendar-128.png',
      subtitle: "Connect to your google calendar",
      default_action: {
        type: 'web_url',
        url: 'calendar.google.com'
      },
      buttons: [
        {
          type: :web_url,
          url: 'calendar.google.com',
          title: 'Connect'
        }
      ]
    },
    {
      title: 'iCalendar',
      # Horizontal image should have 1.91:1 ratio
      image_url: 'http://icons.iconarchive.com/icons/dakirby309/simply-styled/256/Mac-iCal-icon.png',
      subtitle: "Connect to your iCalendar",
      default_action: {
        type: 'web_url',
        url: 'calendar.google.com'
      },
      buttons: [
        {
          type: :web_url,
          url: 'calendar.google.com',
          title: 'Connect'
        }
      ]
    },
    {
      title: 'Outlook',
      # Horizontal image should have 1.91:1 ratio
      image_url: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKEAAAChCAMAAACYjy+EAAAAe1BMVEVVQZr///9GLpNOOJdQO5fOyeHv7vUwCItrXKbHwd7s6/NTPpmnnsn8/P2bkcHz8vdAJZGMgbZeTZ9DKZG2r9OYjcG6tNSups7Dv9haRp5LNJWglsRkUqK/uddbSZ2IfLZ4aqs7H4+CdbPf2+x2ZqzV0eXm4/GAcbQ4GI23qR58AAAFKElEQVR4nO2ceXerKBiHUbCjwSUuaCBuxa3f/xMORjTJTDN37mkkvafvc9r0J/WPpyqLoEUIAAAAAAAAAAAAAAAAAADgZ0CeAd3Pj+LSfQIl2s0x9T3nCXhsL0XRWM/hrRH7GH4MTzK08nQfw78OzzL0jmAIhmAIhr8yDL7I7oZR3sRfoMmj3Q3Zh7jBXkntuyzUx8r1F7b4YG+7G4b2XWFdLrj8mkuVuatzfVOeIDs0bEg58xYcKeg1Izw5S65YjWW1Zm7esM7X+nm26ZYZtZmOQZ7YfqBzlaQvNSS/NqzBEAx/umH4XQ2r1cq3qbfZXg2tKrGLNZ/MtzYItSG7cO4JnbaMiHteMps46cMtm2qxqUYV4rXLxTeZIES2rpjeZPO9Hvrde3Mzhpzl2UKLeaNzPmEa6+K8Rbhd94lrIrMtGzFM+WlrYdK7fnmrNf+oKf41G6kpKV/rbHBv+G1aGzAEw+9laP1php+3Nnki1tbG3F1AnBULvWqxdcxUi73lHuF+3afhpC22bKzXm7vk7Wv9oT6o/rj8oOvnsn35NmQoUo0aIaz5iK85VZkcdZ5HDmu5qZEDjwt/YcJc6qjOOGq2THG/7iPns6yzobN8X1MejWDDNVeJuI5gX9oefqO7gD+pxQbDn2D4ffvl0zKZbQXhbKinpy+zczpfDPU+i+GSjd0FhFW+MAke61hJdSe1ZnUnNa37xDWWOubGZjjrRMMp4jf5Wo7otRxd96lN9cuf3bmLOa9z6vQmq/JtBoCaMUxRvI6mWqL65XUkRtA1U9Lr6Mv69qbfWL+sV2/O9zWl+ndNCVRNeYnhb7U2LzT8ny02GIKhOcPHI9jX1uXk9hg+mJ0zbmij8KSRgrM1N4Ku5Y6kYnL0BuPGDY8EfwZ5uIFvnptLDawveyz8AqzafY3eit6+RBTsbvgM9jJ83rNz1U6GQj5JMGjwPobIZtVTnuFsyE6CCGHxaUPzm4i9jiAAAAAAAAAAAAAAAAAA/AJK7x6y/o+3yvd84fwxSo/fbiayfDjd1/cvUCRuWDnOvJ69gNvx4RvbtZeZnwrEsjs4uTMuTxnOBW0Uf25IaTJkAiFETR5IkhyG8uP4jhJKMeWEzIbqGFKCyXy4ltWb2UhtYjQbEsw5nh+GFAQbcD1mb8lyWGjd+Jnv4oshRfLsT4hy2fe+L9XBxWVcnNvZ0GVFEdeUlk3Nwt0NqQgKvZBgnwfnNDpUKEObq42hi3E5DJU3dIyQshodZxgzOykGbxhDJOSbP3j13oq4t1x97YuYlaixmlQZHuMud91TV5dddOb9MJRpdji7bh5kIombuna60pZWJXu+t6GQVr1WYmGXU2P577Nh3k0cNUGfdAPBqd+1dKwIFuWYCWrjvvUiN5VWn+7fPmLXmnTFFW7VOd5iiE5R13Wj1dZdRVDqj205MvWXzHWZNk7njIGbNgHZb5nkytGq3i/PJePScZhMFkPiDZfXnNRZ9giylaF7mFdt6iGz5VDF/UkZxoYMM4vNbyJjLMfYPpaLYVqpK0+kAm+GaCwEEf0hw/7Qp++VOUNKxiBvJMvjfsySVp/lIzuc+jr2eTmfZWU4vWdRKEovKMR5DGvWmTNUV2I1LwCrnu4cBFFjFe9tENvofAiiqKDu6KjrMIukjXIrGM+jZ7uOFanrtU+ZhYwYIixE4qr+gQrs8vmJOIrVt8C9S7E6xLOE6jsQEar/EJdf1SUV84Pr2Fznp/+LD71tO+jjf+1DXzMGAwAAAAAA+IS/AZ/nzR1SozmfAAAAAElFTkSuQmCC',
      subtitle: "Connect to your Outlook",
      default_action: {
        type: 'web_url',
        url: 'calendar.google.com'
      },
      buttons: [
        {
          type: :web_url,
          url: 'calendar.google.com',
          title: 'Connect'
        }
      ]
    }

  ]

end