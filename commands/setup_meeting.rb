require 'google/apis/calendar_v3'
require 'googleauth'
require 'googleauth/stores/file_token_store'

require 'fileutils'

module SetupMeeting
  module_function

  CONFIRM_TIME = 'CONFIRM_TIME'
  SETUP_MEETING_INVITE_SET = 'SETUP_MEETING_INVITE_SET'

  def meeting_with_who
    @message.typing_on
    contacts = extract_people_by_name(@message.text).map do |conn| format_contact(conn) end
    say "Which #{@message.text}?"
    UI::FBCarousel.new(contacts).send(@user)
  end

  def send_invite
    @postback.payload.slice! CONFIRM_TIME
    say "Finding time with #{@postback.payload} 🔍"

    @postback.typing_on

    user = extract_people_by_name(@postback.payload).first
    user_image = user.photos.first.url
    display_name = user.names.first.display_name

    possible_times = [
        {
          title: "TB Shay / #{display_name}",
          # Horizontal image should have 1.91:1 ratio
          image_url: user_image,
          subtitle: "Tuesday, May 16, 2017\n1:00PM - 1:50PM - No Location",
          default_action: {
            type: 'web_url',
            url: 'calendar.google.com'
          },
          buttons: [
            {
              type: :postback,
              payload: "SETUP_MEETING_INVITE_SET#{display_name}",
              title: 'Send Invite!'
            },
            {
              type: :postback,
              payload: "Continue",
              title: 'Change'
            }
          ]
        }]

    say "Ok! here's the earliest you both can do..."
    UI::FBCarousel.new(possible_times).send(@user)
  end

  def invite_sent
    @postback.payload.slice! SETUP_MEETING_INVITE_SET

    @postback.typing_on

    user = extract_people_by_name(@postback.payload).first
    start_time = Chronic.parse("May 16, 2017 1:00PM")
    end_time = Chronic.parse("May 16, 2017 1:50PM")
    Authorizer.create_meeting(@user, "TB Shay / #{user.names.first.display_name}", start_time, end_time)

    say "Sent! 💥"
    sleep 0.7
    say "I will let you know if Alex denies 🤞"
    UI::ImageAttachment.new(GiphyApi.get_random).send(@user)
  end

  def extract_people_by_name(name)
    service = Google::Apis::PeopleV1::PeopleServiceService.new
    service.authorization = @user.gcreds
    all_users = service.list_person_connections('people/me', { page_size: 500, request_mask_include_field: ['person.names', 'person.email_addresses', 'person.photos'] })

    all_users.connections.select do |conn|
      conn.names.is_a?(Array) &&
        conn.names.first &&
        conn.names.first.display_name &&
        !conn.names.first.display_name.downcase.match(/#{name.downcase}/i).nil?
    end
  end

  def format_contact(contact)
    {
      title: contact.names.first.display_name,
      image_url: contact.photos.first.url,
      default_action: {
        type: 'web_url',
        url: 'calendar.google.com'
      },
      buttons: [
        {
          type: :postback,
          payload: "#{CONFIRM_TIME}#{contact.names.first.display_name}",
          title: "This #{@message.text}"
        }
      ]
    }
  end
end
