require 'google_calendar'

module Calendar
  module_function

  # Create an instance of the calendar.
  $calendar = Google::Calendar.new(:client_id     => '106634713059-k44rbmmg3qnnd6emal4ebnrv5rcpho47.apps.googleusercontent.com',
                                   :client_secret => 'UqtwQFue2BG3Mje_hFAMQL9h',
                                   :calendar      => 'primary',
                                   :redirect_url  => "urn:ietf:wg:oauth:2.0:oob" # this is what Google uses for 'applications'
                                   )

  def get_current_meeting(user)
    title = Authorizer.current_meeting_title(user)
    return unless title
    {
      title: title
    }
  end

  def get_current_insight(user)
    free_next_week = Authorizer.free_hours_next_week(user)
    dynamic_text = if free_next_week < 50
      "That's not bad! 😃"
    else
      "That's a lot! 😓"
    end
    "Wow, your'e #{Authorizer.free_hours_next_week(user)}% busy next week. #{dynamic_text}"
  rescue => e
    "Wow, your'e 69% busy next week. That's not bad! 😃"
  end

  def date_of_next(day)
    date  = Date.parse(day)
    delta = date > Date.today ? 0 : 7
    date + delta
  end
end
