# rubocop:disable Metrics/BlockLength
# require 'dotenv/load' # leave this line commented while working hth heroku
require 'facebook/messenger'
require 'sinatra'
require_relative 'rubotnik/rubotnik'
require_relative 'helpers/helpers'
include Facebook::Messenger
include Helpers # mixing helpers into the common namespace
# so they can be used outside of Dispatches

require 'dotenv/load'
require_relative 'calendar.rb'
require_relative 'giphy_api'

############# START UP YOUR BOT, SET UP GREETING AND MENU ###################

# NB: Subcribe your bot to your page here.
Facebook::Messenger::Subscriptions.subscribe(access_token: ENV['ACCESS_TOKEN'])

# Enable "Get Started" button, greeting and persistent menu for your bot
Rubotnik::BotProfile.enable
Rubotnik::PersistentMenu.enable

############################################################################

# NOTE: QuickReplies.build should be called with a splat operator
# if a set of quick replies is an array of arrays.
# e.g. UI::QuickReplies.build(*replies)
HINTS = UI::QuickReplies.build(['Where am I?', 'LOCATION'],
                               ['Take questionnaire', 'QUESTIONNAIRE'])

ONBOARDING_TEMPLATE_BUTTONS = [
    {
      type: :postback,
      payload: 'FIND_TIME_WITH',
      title: 'Cal, find time with...'
    },
    {
      type: :postback,
      payload: 'MAKE_TIME_FOR',
      title: 'Cali, make time for...'
    },
].freeze

# Build a quick reply that prompts location from user
LOCATION_PROMPT = UI::QuickReplies.location

# Define vartiables you want to use for both messages and postbacks
# outside both Bot.on method calls.
questionnaire_replies = UI::QuickReplies.build(%w[Yes START_QUESTIONNAIRE],
                                               %w[No STOP_QUESTIONNAIRE])
questionnaire_welcome = 'Welcome to the sample questionnaire! Are you ready?'

####################### ROUTE MESSAGES HERE ################################

Bot.on :message do |message|
  # Use DSL inside the following block:
  Rubotnik::MessageDispatch.new(message).route do

    bind 'start' do
    begin
      @message.typing_on if @message
      user_info = get_user_info(:first_name)
      user_name = user_info.nil? ?  "Stranger" : user_info[:first_name]
      @message.typing_off if @message
      say "Hi, #{user_name}!"
      say "We're Cal&Cali, and we're here to help you better manage your work-life balance 8-)"
      url = Authorizer.authorize_url(@user.id)
      p "#{ENV['BASE_APP_URL']}/google/#{@user.id}"
      UI::FBButtonTemplate.new("Before we start please allow us take a look at your calendar",
                             [{
                                "type":"web_url",
                                "url": url,
                                "title":"Connect Calendar"
                              }])
                          .send(@user)
    rescue => e
      say "mmm..something is funky..."
      p "EXCEPTION!!!! #{e.message} #{e.backtrace}"
      p e.message
    end
    end

    bind 'setup', to: :meeting_with_who, start_thread: {
      message: "With whom do you want to meet?"
    }

    bind 'questionnaire', to: :start_questionnaire, start_thread: {
      message: questionnaire_welcome,
      quick_replies: questionnaire_replies
    }

    bind 'where', 'am', 'I', all: true, to: :lookup_location, start_thread: {
      message: 'Let me know your location',
      quick_replies: LOCATION_PROMPT
    }

    # bind 'time', to: :checkkk_show

    # Falback action if none of the commands matched the input,
    # NB: Should always come last. Takes a block.
    bind 'hi', 'hello', 'hey'  do
    begin
      user_info = get_user_info(:first_name)
      user_name = user_info.nil? ?  "Stranger" : user_info[:first_name]
      @message.typing_off if @message
      current_meeting = Calendar.get_current_meeting(@user)
      if current_meeting
        say "Hi, #{user_name}!"
        say "We see you're in '#{current_meeting[:title]}' now. Hope it's going well!"
      else
        say "Hi, #{user_name}!"
        say "How's it going?"
      end

      @message.typing_on if @message

      current_insight = Calendar.get_current_insight(@user)
      @message.typing_off if @message
      say "#{current_insight}"
      @message.typing_on if @message
      @message.typing_off if @message
      UI::FBButtonTemplate.new("How can we help you today? 💪",
                             ONBOARDING_TEMPLATE_BUTTONS)
                        .send(@user)
    rescue => e
        say "mmm..something is funky..."
        p "EXCEPTION!!!! #{e.message} #{e.backtrace}"
        p e.message
    end
    end

    bind 'yo'  do
    begin
      UI::FBButtonTemplate.new("How can we help you today? 💪",
                             ONBOARDING_TEMPLATE_BUTTONS)
                        .send(@user)
    rescue => e
      say "mmm..something is funky..."
      p "EXCEPTION!!!! #{e.message} #{e.backtrace}"
      p e.message
    end
    end


    default do
      say "not sure i understand...."
    end

  end
end

######################## ROUTE POSTBACKS HERE ###############################

Bot.on :postback do |postback|
  Rubotnik::PostbackDispatch.new(postback).route do
    bind 'START' do
      say 'Hello and welcome!'
    end

    bind 'CAROUSEL', to: :show_carousel
    bind 'BUTTON_TEMPLATE', to: :show_button_template
    bind 'IMAGE_ATTACHMENT', to: :send_image

    # Use block syntax when a command takes an argument rather
    # than 'message' or 'user' (which are accessible from everyhwere
    # as instance variables, no need to pass them around).
    bind 'BUTTON_TEMPLATE_ACTION' do
      say "I don't really do anything useful"
    end

    bind 'SQUARE_IMAGES' do
      show_carousel(image_ratio: :square)
    end

    # No custom parameter passed, can use simplified syntax
    bind 'HORIZONTAL_IMAGES', to: :show_carousel

    bind 'LOCATION', to: :lookup_location, start_thread: {
      message: 'Let me know your location',
      quick_replies: LOCATION_PROMPT
    }

    bind 'QUESTIONNAIRE', to: :start_questionnaire, start_thread: {
      message: questionnaire_welcome,
      quick_replies: questionnaire_replies
    }

    bind 'FIND_TIME_WITH', to: :meeting_with_who, start_thread: {
      message: "With whom do you want to meet?"
    }

    bind 'MAKE_TIME_FOR', to: :start_make_time_for, start_thread: {
      message: "What do you want to make time for?"
    }

    bind 'SCHEDULE_MORE_TIME_FOR', to: :stop_make_time_flow

    bind 'SETUP_MEETING_INVITE_SET(.*)', to: :invite_sent
    bind 'CONFIRM_TIME(.*)', to: :send_invite

  end
end

##### USE STANDARD SINATRA TO IMPLEMENT WEBHOOKS FOR OTHER SERVICES #######

# Example of API integration. Use regular Sintatra syntax to define endpoints.
post '/incoming' do
  begin
    sender_id = params['id']
    user = UserStore.instance.find_or_create_user(sender_id)
    say("You got a message: #{params['message']}", user: user)
  rescue
    p 'User not recognized or not available at the time'
  end
end


get '/oauth_callback' do
  creds = Authorizer.authorize(params['state'], params['code'])

  p "FISH:: Get Callback!!!"
  return creds if creds.is_a?(String)
  $google_api.authorization = creds
  @user = UserStore.instance.find(params['state'])

  if @user
    # user_info = get_user_info(:first_name)
    # user_name = user_info.nil? ?  "Stranger" : user_info[:first_name]
    p "FISH:: find user!!!"
    @user.gcreds = creds
    @message.typing_on if @message
    say "hold on while we are learning your work habbits..."
    @message.typing_on if @message
  end
end
