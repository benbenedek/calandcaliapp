module GiphyApi
  module_function

  def get_random
    response = HTTParty.get('http://api.giphy.com/v1/gifs/random?api_key=dc6zaTOxFJmzC&tag=happy')
    parsed = JSON.parse(response.body)
    parsed['data']['image_url']
  end

end