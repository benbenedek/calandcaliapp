require 'google/apis/calendar_v3'
require 'google/apis/people_v1'
require 'googleauth'
require 'googleauth/stores/file_token_store'

require 'fileutils'

$env = 'development'
$config = YAML.load(ERB.new(File.read('./config/config.yml')).result)[$env]

APPLICATION_NAME = 'Cal & Cali'
module Authorizer
  module_function

  CLIENT_SECRETS_PATH = 'client_secret.json'
  CREDENTIALS_PATH = File.join(Dir.home, '.credentials',
                               "ruby-calender.yaml")

  SCOPE = [Google::Apis::CalendarV3::AUTH_CALENDAR, Google::Apis::PeopleV1::AUTH_CONTACTS]

  FileUtils.mkdir_p(File.dirname(CREDENTIALS_PATH))
  client_id = Google::Auth::ClientId.from_file(CLIENT_SECRETS_PATH)
  token_store = Google::Auth::Stores::FileTokenStore.new(file: CREDENTIALS_PATH)
  $authorizer = Google::Auth::UserAuthorizer.new(client_id, SCOPE, token_store, ENV['CALLBACK_URI'])

  def authorize_url(user_id)
    url = $authorizer.get_authorization_url({state: user_id})
    return url
  end

  def authorize(user_id, code)
    credentials = $authorizer.get_credentials_from_code({ user_id: user_id, code: code })
  end

  def create_meeting(user, title, start_time, end_time)
    $calendar.login_with_refresh_token(user.gcreds.refresh_token)

    start_time = start_time + (4 * 60 * 60) || Time.now + (4 * 60 * 60) # seconds * min
    end_time = start_time + (60 * 60) # seconds * min

    event = $calendar.create_event do |e|
      e.title = title || 'A Cool Event with Rachel'
      e.start_time = start_time
      e.end_time = end_time
    end
  end

  def count_overlaps_meetings(user, start_date, end_date)
    return unless user.gcreds

    refresh_token = user.gcreds.refresh_token
    $calendar.login_with_refresh_token(refresh_token)
    start_time = start_date.to_time + (4*60*60)
    end_time = start_time + 60*60
    events = $calendar.find_events_in_range(start_time, end_time , {max_results: 10})

    return events.count
  rescue => e
    1
  end

  def cancel_overlaps_meetings(user, start_date, end_date)
    return unless user.gcreds

    refresh_token = user.gcreds.refresh_token
    $calendar.login_with_refresh_token(refresh_token)
    start_time = start_date.to_time + (4*60*60)
    end_time = start_time + 60*60
    events = $calendar.find_events_in_range(start_time, end_time, {max_results: 10})

    return unless events && events.any?

    # Remove All day events. dont want to delete them
    events.delete_if { |e| e.raw['start']['dateTime'].nil? }

    events.each { |event| $calendar.delete_event(event) }
    events.count
  end

  def current_meeting_title(user)
    return unless user.gcreds

    refresh_token = user.gcreds.refresh_token
    $calendar.login_with_refresh_token(refresh_token)
    start_date = Time.now - 1
    end_date = Time.now + 1
    events = $calendar.find_events_in_range(start_date.to_time, end_date.to_time, {max_results: 1})
    return unless events
    title = events.first.title
  end

  def free_hours_next_week(user)
    return unless user.gcreds

    refresh_token = user.gcreds.refresh_token
    $calendar.login_with_refresh_token(refresh_token)

    start_of_next_week = Calendar.date_of_next('Monday')
    end_of_next_week = start_of_next_week.next_day(5)

    events = $calendar.find_events_in_range(start_of_next_week.to_time, end_of_next_week.to_time, {max_results: 200})
    events.delete_if { |e| e.raw['start']['dateTime'].nil? }
    return unless events.any?

    timezone = DateTime.parse(events.first.raw['start']['dateTime']).zone

    $freebusy = Google::Freebusy.new(
              :client_id     => '106634713059-k44rbmmg3qnnd6emal4ebnrv5rcpho47.apps.googleusercontent.com',
             :client_secret => 'UqtwQFue2BG3Mje_hFAMQL9h',
             :calendar      => 'primary',
             :refresh_token => refresh_token
          )

    busy_time = $freebusy.query(['primary'], start_of_next_week.to_time, end_of_next_week.to_time)
    busy_hours = 0
    busy_time['primary'].each do |range|
      st = DateTime.parse(range['start']).new_offset(timezone).strftime('%H%M')
      new_st = st < '0800' ? '0800' : st

      et = DateTime.parse(range['end']).new_offset(timezone).strftime('%H%M')
      new_et = et > '1800' ? '1800' : et

      diff_time = new_et.to_i - new_st.to_i
      busy_hours += diff_time / 100 if diff_time > 0
      busy_hours += (diff_time % 100) / 60.0 if diff_time > 0

      p "#{st} #{et} | #{new_st} #{new_et} diff: #{new_et.to_i - new_st.to_i} | #{busy_hours}"
    end

    ((busy_hours / 50.0) * 100).round(0)
  end
end

# Fetch the next 10 events for the user
service = Google::Apis::CalendarV3::CalendarService.new
# $google_api.authorization = Authorizer.authorize(@user.id)
service.client_options.application_name = APPLICATION_NAME
# calendar_id = 'primary'
$google_api = service
